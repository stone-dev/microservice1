using System.Net.Sockets;
using NUnit.Framework;
using Microservice1;

namespace Microservice1.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Test]
        public void TestAdd()
        {
            var wf = new Microservice1.WeatherForecast();
            Assert.AreEqual(wf.Add(1,1), 2);
            Assert.AreEqual(wf.Add(2,2), 4);
        }
    }
}